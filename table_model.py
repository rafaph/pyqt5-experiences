# -*- coding: utf-8 -*-
from PyQt5.QtCore import QAbstractTableModel, Qt, QVariant


class TableModel(QAbstractTableModel):
    def __init__(self, parent):
        super(TableModel, self).__init__(parent)
        self.initialData = [{
            'id': 1,
            'prof_ini': 0.0,
            'prof_fin': 50.0,
            'extension': 50.0
        }, {
            'id': 2,
            'prof_ini': 50.0,
            'prof_fin': 100.0,
            'extension': 50.0
        }]

        self.map = ['prof_ini', 'prof_fin', 'extension']

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self.initialData)

    def columnCount(self, parent=None, *args, **kwargs):
        return 3

    def data(self, index, role=None):
        if role == Qt.DisplayRole:
            return self.initialData[index.row()][self.map[index.column()]]
        return QVariant()
